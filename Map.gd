extends Node2D

@onready
var held_token: Node2D = null

@onready
var grabbed_tokens := {}

# Called when the node enters the scene tree for the first time.
func _ready():
	for token in get_tree().get_nodes_in_group('token'):
		if token is Area2D:
			token.input_event.connect(self._on_token_input_event.bind(token))

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_RIGHT:
		if event.pressed and is_multiplayer_authority():
			$TokenSpawner.spawn(event.position)

func _on_token_input_event(_viewport, event: InputEvent, _shape_idx, token: Node2D):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		if event.pressed:
			rpc('grab_token', token.get_path())
		else:
			rpc('release_token', token.get_path())

func _process(_delta) -> void:
	if held_token != null:
		var center_pos := get_viewport_rect().size / 2
		var mouse_pos := get_viewport().get_mouse_position()
		var camera_pos := $Camera.position as Vector2
		held_token.position = camera_pos + (mouse_pos - center_pos)

@rpc(any_peer, call_local)
func grab_token(token_path):
	var token := get_node(token_path) as Node
	var peer_id := multiplayer.get_remote_sender_id()
	if token.get_multiplayer_authority() == 1:
		token.set_multiplayer_authority(peer_id)
		if multiplayer.get_unique_id() == peer_id:
			held_token = token

@rpc(any_peer, call_local)
func release_token(token_path):
	var peer_id := multiplayer.get_remote_sender_id()
	var token := get_node(token_path) as Node
	if token.is_multiplayer_authority():
		held_token = null
	if token.get_multiplayer_authority() == multiplayer.get_remote_sender_id():
		token.set_multiplayer_authority(1)
