extends MultiplayerSpawner

@export var token_scn: PackedScene

func _spawn_custom(data):
	if not data is Vector2:
		return null
	var token := token_scn.instantiate() as Area2D
	var map := get_node(spawn_path) as Node
	token.position = data
	token.input_event.connect(map._on_token_input_event.bind(token))
	return token
