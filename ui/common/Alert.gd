extends PanelContainer


func set_text(text: String):
	$Label.text = text


func _on_alert_gui_input(event):
	if event is InputEventMouseButton and event.pressed and\
			event.button_index == MOUSE_BUTTON_LEFT:
		$AnimationPlayer.play("fade_out")
