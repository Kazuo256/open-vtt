extends MarginContainer

signal back_pressed


func _ready():
	Network.host_succeeded.connect(self._on_network_host_succeeded)


func _on_back_button_pressed():
	back_pressed.emit()


func _on_host_button_pressed():
	var port : int =\
			$Panel/Contents/HBoxContainer/VBoxContainer2/Port.text.to_int()
	Network.host(port)


func _on_network_host_succeeded():
	get_tree().change_scene("res://ui/session_view/Session.tscn")
