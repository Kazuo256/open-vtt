extends PanelContainer

signal join_pressed
signal host_pressed
signal quit_pressed


func _on_join_pressed():
	join_pressed.emit()


func _on_host_pressed():
	host_pressed.emit()


func _on_quit_pressed():
	quit_pressed.emit()
