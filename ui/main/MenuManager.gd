extends Control

@export var menus : Array[NodePath]


func go_to_menu(menu: Control):
	menu.show()
	for path in menus:
		if get_node(path) != menu:
			(get_node(path) as Control).hide()


func _on_main_menu_host_pressed():
	go_to_menu($HostSetup)


func _on_main_menu_join_pressed():
	go_to_menu($JoinMenu)


func _on_main_menu_quit_pressed():
	get_tree().quit()


func _on_host_setup_back_pressed():
	go_to_menu($MainMenu)


func _on_join_menu_back_pressed():
	go_to_menu($MainMenu)
