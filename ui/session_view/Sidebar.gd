@tool
extends HBoxContainer

enum Side {
	LEFT, RIGHT
}

@export var side: Side:
	get:
		return side
	set(value):
		side = value
		refresh()

@export var tab_scenes: Array[PackedScene]

@export var button_tab_scene: PackedScene

@onready var tabs := {}

@onready var tab_group := ButtonGroup.new()

func _ready():
	for tab_scene in tab_scenes:
		var tab := tab_scene.instantiate() as SidebarContents
		var tab_button := button_tab_scene.instantiate() as Button
		tab_button.text = tab.tab_name
		tab_button.button_group = tab_group
		$Tabs.add_child(tab_button)
		
		tabs[tab.tab_name] = tab
	
	tab_group.pressed.connect(self._on_tab_toggled)
	refresh()

func refresh():
	if is_inside_tree():
		match side:
			Side.LEFT:
				move_child($Tabs, 2)
				move_child($Panel, 0)
				alignment = BoxContainer.ALIGNMENT_BEGIN
			Side.RIGHT:
				move_child($Tabs, 0)
				move_child($Panel, 2)
				alignment = BoxContainer.ALIGNMENT_END

func _on_tab_toggled(button: Button):
	match side:
		Side.LEFT:
			alignment = BoxContainer.ALIGNMENT_END
			$Collapse.text = '<'
		Side.RIGHT:
			alignment = BoxContainer.ALIGNMENT_BEGIN
			$Collapse.text = '>'
	
	if $Panel/ScrollContainer.get_child_count():
		$Panel/ScrollContainer.remove_child($Panel/ScrollContainer.get_child(0))
	
	$Panel/ScrollContainer.add_child(tabs[button.text])

func _on_collapse_pressed():
	for tab in $Tabs.get_children():
		tab.button_pressed = false
	
	match side:
		Side.LEFT:
			alignment = BoxContainer.ALIGNMENT_BEGIN
		Side.RIGHT:
			alignment = BoxContainer.ALIGNMENT_END
