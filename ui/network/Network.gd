extends Control

func _ready():
	$MenuDock.show_unconnected_mode()

func _on_host():
	if Network.host():
		$MenuDock.show_connected_mode()

func _on_join():
	if Network.join("localhost"):
		$MenuDock.show_connected_mode()
