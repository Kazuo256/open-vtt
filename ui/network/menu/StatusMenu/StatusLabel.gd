extends Label

const STATUS := [
	'Disconnected',
	'Connecting',
	'Connected'
]

func _process(_delta):
	var peer := multiplayer.multiplayer_peer
	if peer != null:
		text = STATUS[peer.get_connection_status()]
	else:
		text = "No peer"
