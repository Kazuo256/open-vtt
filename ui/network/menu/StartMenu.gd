extends PanelContainer

signal joined
signal hosted

func join():
	emit_signal("joined")

func host():
	emit_signal("hosted")
