extends Control

@export var alert_scene : PackedScene

func show_alert(message: String):
	var alert = alert_scene.instantiate()
	alert.set_text(message)
	$Alerts.add_child(alert)
